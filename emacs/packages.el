;;; packages.el --- steven Layer packages File for Spacemacs
;;
;; Copyright (c) 2012-2014 Sylvain Benner
;; Copyright (c) 2014-2015 Sylvain Benner & Contributors
;;
;; Author: Sylvain Benner <sylvain.benner@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;; List of all packages to install and/or initialize. Built-in packages
;; which require an initialization must be listed explicitly in the list.
(setq steven-packages
    '(
      css-mode
      bash-completion
      lispy
      company
      flycheck
      switch-window
      markdown-mode
      slime
      impatient-mode
      swiper
      paredit
      magit
      git-messenger
      helm-flyspell
      helm
      helm-ls-git
      flycheck-package
      js2-mode
      visual-regexp
      visual-regexp-steroids
      helm-gtags
      json-mode
      racket-mode
      yasnippet
      evil-commentary
      django-mode
      helm-ag
      hungry-delete
      clojure-mode
      find-file-in-project
      hl-anything
      projectile
      wrap-region
      ctags-update
      evil-vimish-fold
      beacon
      evil-visual-mark-mode
      js-doc
      org-octopress
      ))

;; List of packages to exclude.
(setq steven-excluded-packages '())

;; For each package, define a function steven/init-<package-name>
;;
;; (defun steven/init-my-package ()
;;   "Initialize my package"
;;   )
;;
;; Often the body of an initialize function uses `use-package'
;; For more info on `use-package', see readme:
;; https://github.com/jwiegley/use-package
(defun steven/init-slime ()
  (use-package slime)
  ;; (setq inferior-lisp-program "/usr/local/bin/sbcl")
  (setq inferior-lisp-program "/usr/local/bin/clisp")
  (setq slime-contribs '(slime-fancy))
  )

(defun steven/init-paredit ()
  (use-package paredit)
  )

(defun steven/init-bash-completion ()
  (use-package bash-completion)
  )

(defun steven/init-switch-window ()
  (use-package switch-window)
  )

(defun steven/init-lispy ()
  (use-package lispy)
  )

(defun steven/init-clojure-mode ()
  (use-package clojure-mode)
  )

(defun steven/init-evil-commentary ()
  (use-package evil-commentary)
  )

(defun steven/init-django-mode ()
  (use-package django-mode)
  )

(defun steven/init-org-octopress ()
  (use-package org-octopress)
  )

;;initialize org-octopress and setup the project, not working now.
;; (defun steven/init-org-octopress ()
;;   (use-package org-octopress
;;     :init
;;     (progn
;;       (add-hook 'org-octopress-summary-mode-hook
;;                 #'(lambda () (local-set-key (kbd "q") 'bury-buffer)))
;;       (setq org-blog-dir "~/blog/octopress/")
;;       (setq org-octopress-directory-top org-blog-dir)
;;       (setq org-octopress-directory-posts (concat org-blog-dir "source/_posts"))
;;       (setq org-octopress-directory-org-top org-blog-dir)
;;       (setq org-octopress-directory-org-posts (concat org-blog-dir "source/blog"))
;;       (setq org-octopress-setup-file (concat org-blog-dir "setupfile.org"))
;;       )))

;;for paredit mode
;; (autoload 'paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
;; (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
;; (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
;; (add-hook 'ielm-mode-hook             #'enable-paredit-mode)
;; (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
;; (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)

(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))
