runtime bundle/vim-pathogen/autoload/pathogen.vim
"filetype off
"call pathogen#incubate()
"filetype plugin indent on
"call pathogen#infect()
"call pathogen#helptags()
set guifont=Monaco:h14
"powerline
"if has("gui_running")
   "let s:uname = system("uname")
   "if s:uname == "Darwin\n"
      "set guifont=Inconsolata\ for\ Powerline:h15
   "endif
"endif

"set guifont=Inconsolata\ for\ Powerline:h28
let g:Powerline_symbols = 'fancy'
set encoding=utf-8
set t_Co=256
set fillchars+=stl:\ ,stlnc:\
set term=xterm-256color
set termencoding=utf-8
execute pathogen#infect()
syntax on
filetype plugin indent on
" hit enter to cancel searched highlight
noremap <CR> :nohlsearch<CR>

" select ALL
map <C-A> ggVG
