# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
autoload zmv
export VISUAL='/usr/local/bin/emacs'
#export TERM="screen-256color"

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"
ZSH_THEME="candy"
#ZSH_THEME="bureau"
#ZSH_THEME="bira"
#ZSH_THEME="dracula"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=60
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git perl ruby vim mvim emacs php textmate mate colored-man-pages colorize command-not-found compleat copydir copyfile cp history pass per-directory-history tmux jump cake coffee cpanm github bundler gem rake fabric django virtualenv pip osx brew themes)
#alias rake="bundle exec rake"
alias generate="bundle exec rake generate"
alias preview="bundle exec rake preview"
alias deploy="bundle exec rake deploy"

# for git
alias git-master="git push -u origin master"
alias git-source="git push -u origin source"
alias perli="perl -Ilib"
alias reperl="perl -de0"
export PERLDOC="-MPod::Text::Color::Delight"
source $ZSH/oh-my-zsh.sh
alias emacs=/usr/local/bin/emacs2
alias vim=mvim
alias bs1="ssh stsun@10.30.40.31 -D 1080"
alias bs2="ssh stsun@10.30.40.32 -D 1080"
#alias bs1="mosh stsun@10.30.40.31 -p 1080 --server=/usr/local/bin/mosh-server"
#alias bs2="mosh stsun@10.30.40.32 -p 1080"
alias ll='ls -alF'
alias la='ls -A'
alias lsd='ls -d */'
alias sl='ls'
alias l='ls -CF'
alias spacemacs='open -a /Applications/Emacs.app $1'
alias smacs='open -a /Applications/Emacs.app $1'
#alias emacs='/usr/local/Cellar/emacs-mac/emacs-24.5-z-mac-5.7/Emacs.app $1'
# alias emacs="emacs 2>/dev/null"
#alias vim="mvim 2>/dev/null"
alias vsb='vs --local-port 0'
alias uu="sudo apt-get update"
alias pg="ps aux | grep vpnc"
alias rr="sudo reboot"
alias ll='ls -l'
#alias vi='vim'
alias -s gz='tar -xzvf'
alias -s tgz='tar -xzvf'
alias -s zip='unzip'
alias -s bz2='tar -xjvf'
alias rx=rxrx
##alias for cd
alias cd..2='cd ../..'
alias cd..3='cd ../../..'
alias cd..4='cd ../../../..'
alias cd..5='cd ../../../../..'
alias cd..6='cd ../../../../../..'

##mysql
alias mysql="/usr/local/mysql/bin/mysql"
#####
export PATH="/usr/bin:/bin:/usr/sbin:/sbin:/usr/texbin:/usr/local/mysql/bin"
export PATH="$HOME/local/bin:/Users/sun/rakudo/install/bin:$PATH:$HOME/bin"
export PERL5LIB=/Users/sun/local/bin:/Users/sun/perl5

# export LANG=en_US.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

export CLASSPATH=$CLASSPATH:~/algs4/stdlib.jar:~/algs4/algs4.jar:~/algs4/introcs.jar:~/algs4

### Added by the Heroku Toolbelt
export PATH=$PATH:/usr/local/bin:/Users/sun/rakudo/install/languages/perl6/site/bin
export PATH=$PATH:/usr/local/opt/go/libexec/bin

#source ~/perl5/perlbrew/etc/bashrc
##pyenv
export PYENV_ROOT="$HOME/.pyenv"
export RBENV_ROOT="$HOME/.rbenv"
export PLENV_ROOT="$HOME/.plenv"
export PATH=~/.rakudobrew/bin:$PATH
export PATH="$RBENV_ROOT/bin:$PYENV_ROOT/bin:$PLENV_ROOT/bin:$PATH"
##for virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh
eval "$(rbenv init -)"
eval "$(pyenv init -)"
eval "$(plenv init -)"
eval $(thefuck --alias fuck)
[[ -s /usr/local/etc/profile.d/autojump.sh ]] && . /usr/local/etc/profile.d/autojump.sh
